package com.timewastingguru.domain.interactor

import com.timewastingguru.domain.executor.PostExecutionThread
import com.timewastingguru.domain.executor.ThreadExecutor

abstract class ParameterizedInteractor<Param, Result>(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : Interactor<Result>(threadExecutor, postExecutionThread) {

    abstract fun get(param: Param): Result

}