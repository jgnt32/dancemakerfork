package com.timewastingguru.domain.interactor

import com.timewastingguru.domain.executor.PostExecutionThread
import com.timewastingguru.domain.executor.ThreadExecutor

abstract class Interactor<Result> (val threadExecutor: ThreadExecutor, val postExecutionThread: PostExecutionThread) {


}